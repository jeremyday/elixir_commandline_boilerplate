defmodule ElixirCommandlineBoilerplate.Mixfile do
  use Mix.Project

  def project do
    [
      app:             :elixir_commandline_boilerplate,
      build_embedded:  Mix.env == :prod,
      deps:            deps(),
      elixir:          "~> 1.3",
      escript:         escript_config,
      name:            "Elixir Commandline Boilerplate",
      source_url:      "https://gitlab.com/jeremyday/elixir_commandline_boilerplate",
      start_permanent: Mix.env == :prod,
      version:         "0.1.0"
    ]
  end

  # Configuration for the OTP application
  #
  # Type "mix help compile.app" for more information
  def application do
    [ applications: [ :logger ] ]
  end

  # Dependencies can be Hex packages:
  #
  #   {:mydep, "~> 0.3.0"}
  #
  # Or git/path repositories:
  #
  #   {:mydep, git: "https://github.com/elixir-lang/mydep.git", tag: "0.1.0"}
  #
  # Type "mix help deps" for more examples and options
  defp deps do
    [
      { :logger_file_backend, "~> 0.0.9" }
    ]
  end

  defp escript_config do
    [ main_module: CLI ]
  end
end
