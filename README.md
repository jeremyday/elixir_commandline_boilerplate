# ElixirCommandlineBoilerplate

Simple boilerplate for setting up a command line Elixir application. This code comes largely from chapter 13 of [Programming Elixir 1.3](https://pragprog.com/book/elixir13/programming-elixir-1-3), by Dave Thomas.

## Installation

```shell
git clone git@gitlab.com:jeremyday/elixir_commandline_boilerplate.git
cd elixir_commandline_boilerplate
mix deps.get
mix escript.build
./elixir_commandline_boilerplate
```

## Configuration

If you copy this code into another directory you'll need to:

1. Rename all instances of `ElixirCommandlineBoilerplate` to your new application name.
2. Rename `lib/elixir_commandline_boilerplate.ex`.
3. Rename `test/elixir_commandline_boilerplate_test.exs`.
4. Update the `:app`, `:name`, and `:source_url` values in `project/0` in `mix.exs`.
5. In `lib/cli.ex` you'll need to update `parse_arguments/1` to parse out any commands (e.g., "test") into unique atoms (e.g., `:test`).
6. In `lib/cli.ex` you'll also need to update `process/1` to do something for each of your command atoms (e.g., `:test`).
