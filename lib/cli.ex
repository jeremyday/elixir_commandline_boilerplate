defmodule CLI do
  @moduledoc """
  Parse the command line and dispatch to handler functions.
  """

  require Logger

  @doc """
  Process the command line arguments and run the application.

  `argv` - The command line arguments.
  """
  def run(argv) do
    argv
    |> parse_args
    |> process_commands
  end

  @doc """
  Function that is called automatically by `escript` when the application is run from the command line.

  `argv` - The command line arguments.
  """
  def main(argv) do
    run argv
  end

  @doc """
  Parse the command line arguments.

  `argv` - The command line arguments.
           "-h" or "--help" returns [:help].
           "test" returns [:text].

  Returns an array of atoms based on `argv`.
  """
  def parse_args(argv) do
    # parse contains {parsed, argv, errors}
    parse = OptionParser.parse List.flatten([argv]),
                               switches: [ help: :boolean ],
                               aliases:  [ h:    :help ]

    case parse do
      { [help: true], _, _ } ->
        :help
      { _, arguments, _} ->
        parse_arguments(arguments)
        |> Enum.uniq
    end
  end

  @doc """
  Display the help prompt, since no valid commands were entered.
  """
  def process_commands(commands) when length(commands) == 0 do
    process :help
  end

  @doc """
  Process commands.

  `commands` - Commands to process.
  """
  def process_commands(commands) do
    process commands
  end

  defp parse_arguments([head | tail]) do
    Enum.concat parse_arguments(head), parse_arguments(tail)
  end

  defp parse_arguments("test"), do: [:test]

  defp parse_arguments([]), do: []

  defp parse_arguments(_), do: []

  defp process(:help) do
    IO.puts """
    usage:  elixir_commandline_boilerplate test [-h --help]
    """
  end

  defp process(:test), do: Logger.debug 'This is some test output.'

  defp process([ head | tail ]) do
    process head
    process tail
  end

  defp process([]) do end

  defp process(argument), do: Logger.error("Unknown argument '#{argument}'.")
end
