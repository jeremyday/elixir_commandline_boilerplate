defmodule CliTest do
  use ExUnit.Case
  doctest ElixirCommandlineBoilerplate
  import CLI, only: [ parse_args: 1 ]

  test ":help returned by option parsing with -h and --help options" do
    assert parse_args(["-h",     "something"]) == :help
    assert parse_args(["--help", "something"]) == :help
  end

  test "[:test] returned by option parsing with 'test' parameter" do
    assert parse_args(["test", "test2"]) == [:test]
  end
end
